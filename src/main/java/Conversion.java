public class Conversion {
    public static String removePrefix(String s, String prefix)
    {
        if (s != null && prefix != null && s.startsWith(prefix)){
            return s.substring(prefix.length());
        }
        return s;
    }

    public static void main(String[] args) {
        String s = "supernaturalThingsIsOne";
        String s1=toCamelCase(removePrefix(s, "super"));
        System.out.println(s1);
    }
    public static String toCamelCase(String s) {
        String[] tokens = s
                .replaceAll("`|'", "")
                .split("[\\W_]+|(?<=[a-z])(?=[A-Z][a-z])");
        s = "";
        for (String token : tokens) {
            String lowercaseToken = token.toLowerCase();
            s += tokens[0].equals(token)?lowercaseToken:lowercaseToken.toUpperCase().charAt(0) + lowercaseToken.substring(1); // 4. Uppercase first char of all but first word
        }
        return s;
    }
}
